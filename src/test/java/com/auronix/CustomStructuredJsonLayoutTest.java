package com.auronix;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.log4j.Category;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomStructuredJsonLayoutTest {

  private static class FakeCategory extends Category {
    private FakeCategory(final String name) {
      super(name);
    }
  }

  static Stream<Arguments> formattedOutputExamples() {
    return Stream
        .of(
            Arguments.of("info", "com.auronix.service.A", "2024-05-10T12:30:00Z", "main", "hello world!",
                (Supplier<Throwable>) () -> null,
                "{\"level\":\"INFO\",\"logger\":\"com.auronix.service.A\",\"time\":1715344200,\"thread\":\"main\",\"message\":\"hello world!\"}\n"),
            Arguments.of("info", "com.auronix.service.A", "2024-05-10T12:30:00Z", "main", "hello\n\tworld!",
                (Supplier<Throwable>) () -> null,
                "{\"level\":\"INFO\",\"logger\":\"com.auronix.service.A\",\"time\":1715344200,\"thread\":\"main\",\"message\":\"hello\\n\\tworld!\"}\n"),
            Arguments.of("warn", "com.auronix.service.B", "2024-05-10T12:31:00Z", "this-is-a-thread",
                "hello world!", (Supplier<Throwable>) () -> {
                  var th = new Throwable("Boom!");
                  th.setStackTrace(
                      List.of(
                          new StackTraceElement("TheClass", "method1", "theclass.java", 10),
                          new StackTraceElement("OuterClass", "method2", "outerclass.java", 22))
                          .toArray(new StackTraceElement[0]));
                  return th;
                },
                "{\"level\":\"WARN\",\"logger\":\"com.auronix.service.B\",\"time\":1715344260,\"thread\":\"this-is-a-thread\",\"stackTrace\":\"java.lang.Throwable: Boom!\\n\\tat TheClass.method1(theclass.java:10)\\n\\tat OuterClass.method2(outerclass.java:22)\",\"message\":\"java.lang.Throwable: Boom!\"}\n"));
  }

  @ParameterizedTest(name = "[{index}] - formatted output")
  @MethodSource("formattedOutputExamples")
  void testFormattedOutput(String level, String logger, String timestamp, String thread, String msg,
      Supplier<Throwable> thSupplier,
      String expected)
      throws Exception {
    // arrange
    var layout = new CustomStructuredJsonLayout();
    var loggingEvent = new LoggingEvent("fcqn", new FakeCategory(logger),
        LocalDateTime.parse(timestamp, DateTimeFormatter.ISO_DATE_TIME).toEpochSecond(ZoneOffset.UTC),
        Level.toLevel(level), msg, thread,
        Optional.ofNullable(thSupplier.get()).map(x -> new ThrowableInformation(x)).orElse(null), null, null, null);

    // act
    var jsonLog = layout.format(loggingEvent);

    // assert
    assertThat(jsonLog).isEqualTo(expected);
  }
}
