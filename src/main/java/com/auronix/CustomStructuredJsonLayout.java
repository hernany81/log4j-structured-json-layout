package com.auronix;

import java.util.Optional;

import org.apache.log4j.spi.LoggingEvent;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.json.JsonWriteFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.confluent.common.logging.log4j.StructuredJsonLayout;

/**
 * CustomStructuredJsonLayout
 */
public class CustomStructuredJsonLayout extends StructuredJsonLayout {

  private static class LogRecord {
    @JsonProperty("level")
    String level;
    @JsonProperty("logger")
    String logger;
    @JsonProperty("time")
    Long timeMs;
    @JsonProperty("thread")
    String thread;
    @JsonProperty("stackTrace")
    String stackTrace;

    @JsonProperty("message")
    String message;

    private LogRecord(
        final String level,
        final String logger,
        final Long timeMs,
        final String thread,
        final String stackTrace,
        final String message) {
      this.level = level;
      this.logger = logger;
      this.timeMs = timeMs;
      this.thread = thread;
      this.stackTrace = stackTrace;
      this.message = message;
    }
  }

  private ObjectMapper objectMapper;

  public CustomStructuredJsonLayout() {
    this.objectMapper = new ObjectMapper();
    this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    this.objectMapper.writer().with(JsonWriteFeature.ESCAPE_NON_ASCII);
  }

  @Override
  public String format(LoggingEvent loggingEvent) {
    var throwableInfo = Optional.ofNullable(loggingEvent.getThrowableInformation());

    var record = new LogRecord(
        loggingEvent.getLevel().toString(),
        loggingEvent.getLoggerName(),
        loggingEvent.getTimeStamp(),
        loggingEvent.getThreadName(),
        throwableInfo.map(x -> String.join("\n", x.getThrowableStrRep())).orElse(null),
        throwableInfo.map(x -> x.getThrowableStrRep()[0]).orElse(loggingEvent.getRenderedMessage()));

    try {
      return new StringBuilder(objectMapper.writeValueAsString(record))
          .append("\n")
          .toString();
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public boolean ignoresThrowable() {
    return false;
  }

}