# Log4j structured JSON Layout

This is a project to extend the functionality provided by the  [StructuredJsonLayout](https://github.com/confluentinc/common/blob/9253bdb6106b60a411ee90e9dbe8a8f1640d05ff/log4j-extensions/src/main/java/io/confluent/common/logging/log4j/StructuredJsonLayout.java) implementation and log additional properties.

## Create a release

```bash
git tag v1.0.0
git push origin v1.0.0
```
